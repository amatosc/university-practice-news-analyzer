package io.newsAnalyzer.models;

import junit.framework.TestCase;

public class NewsTest extends TestCase {
	
	private News news;
	
	protected void setUp() throws Exception {
		super.setUp();
		
		news = new News("Test");
	}

	public void testAddEntity() {
		news.addEntity(new NamedEntity("1"));
		news.addEntity(new NamedEntity("1"));
		news.addEntity(new NamedEntity("1"));
		news.addEntity(new NamedEntity("1"));
		
		news.addEntity(new NamedEntity("2"));
		
		news.addEntity(new NamedEntity("3"));
		news.addEntity(new NamedEntity("3"));
		
		Iterable<NamedEntity> tmp = news.getEntityList();
		
		int i = 0;
		for (NamedEntity namedEntity : tmp) {
			if(namedEntity.getName().equals("1"))
				assertEquals(4, namedEntity.getFrequency());
			
			if(namedEntity.getName().equals("2"))
				assertEquals(1, namedEntity.getFrequency());
			
			if(namedEntity.getName().equals("3"))
				assertEquals(2, namedEntity.getFrequency());
			
			i++;
		}
		
		
		assertEquals((Integer) i,(Integer) 3);
	}

}
