package io.newsAnalyzer.models;

public class NamedEntity {

	private String name;
	private int frequency;

	public NamedEntity(String name) {
		this.name = name;
		frequency = 1;
	}

	public NamedEntity(String name, int frequency) {
		this.name = name;
		this.frequency = frequency;
	}

	public String getName() {
		return name;
	}

	@Override
	public String toString() {
		return "NamedEntity [name=" + name + ", frequency=" + frequency + "]";
	}

	public int getFrequency() {
		return frequency;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NamedEntity other = (NamedEntity) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

}
