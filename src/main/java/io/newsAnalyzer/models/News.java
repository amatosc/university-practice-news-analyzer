package io.newsAnalyzer.models;

import io.newsAnalyzer.material.HashTableMap;
import io.newsAnalyzer.material.LinearHashing;


public class News {

	private String title;
	private HashTableMap<String, NamedEntity> entityList;
	private NamedEntity max;

	public News() {
		title = "";
		entityList = new LinearHashing<>();
		max = null;
	}

	public News(String title) {
		this.title = title;
		this.entityList = new LinearHashing<>();
		this.max = null;
	}

	public News(String title, Iterable<NamedEntity> entities) {
		this(title);
		for (NamedEntity e : entities)
			addEntity(e);

	}

	public String getTitle() {
		return title;
	}

	public Iterable<NamedEntity> getEntityList() {
		return entityList.values();
	}

	public NamedEntity getEntityWithMoreFrequency() {
		return max;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void addEntity(NamedEntity entity) {
		NamedEntity tmp = entityList.get(entity.getName());

		NamedEntity insert = entity;

		if (tmp != null)
			insert = new NamedEntity(tmp.getName(), tmp.getFrequency() + 1);
		
		if(max == null)
			max = insert;
		else if(insert.getFrequency() > max.getFrequency())
			max = insert;
		
		entityList.put(insert.getName(), insert);
	}

	@Override
	public String toString() {
		return "News [title=" + title + ", entityList=" + entityList + ", max="
				+ max + "]";
	}

}
