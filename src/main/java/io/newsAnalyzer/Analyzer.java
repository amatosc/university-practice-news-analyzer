package io.newsAnalyzer;

import static java.nio.charset.StandardCharsets.UTF_8;
import io.newsAnalyzer.material.HashTableMap;
import io.newsAnalyzer.material.LinearHashing;
import io.newsAnalyzer.models.NamedEntity;
import io.newsAnalyzer.models.News;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.StringTokenizer;

public class Analyzer {

	private HashSet<String> stoplist;

	// Map relation of Max named entity and news.
	private HashTableMap<NamedEntity, List<News>> allNwes;

	public Analyzer() throws IOException {
		// load the list of nonwords.
		load_ESstopList();

		// Analyzer all news.
		analyzerAllNwes();

		// Show Data.
		printInf();

	}

	/**
	 * Print all news.
	 */
	private void printInf() {
		Iterable<NamedEntity> keys = allNwes.keySet();
		List<News> list;

		for (NamedEntity k : keys) {
			list = allNwes.get(k);

			System.out.println(k.getName());
			for (News news : list) {
				System.out.println("**[" + news.getTitle() + "]");
			}
		}
	}

	protected void analyzerAllNwes() throws IOException {
		allNwes = new LinearHashing<>();
		File file = new File(this.getClass().getResource("../../news/")
				.getPath());

		News tmpNews;
		List<News> values;

		for (File f : file.listFiles()) {
			if (f.isFile()) {
				tmpNews = analyzerNews(f);
				NamedEntity key = tmpNews.getEntityWithMoreFrequency();
				values = allNwes.get(key);

				if (values == null)
					values = new ArrayList<>();

				values.add(tmpNews);

				allNwes.put(key, values);
			}
		}
	}

	/**
	 * Load the list of nonwords.
	 */
	private void load_ESstopList() {
		stoplist = new HashSet<>();

		File f = new File(this.getClass().getResource("../../ES_stopList.txt")
				.getFile());

		try {
			Reader r = new FileReader(f);
			BufferedReader br = new BufferedReader(r);
			String line;

			while ((line = br.readLine()) != null) {
				StringTokenizer t = new StringTokenizer(line, " ");

				while (t.hasMoreElements()) {
					byte tmp[] = t.nextElement().toString().getBytes(UTF_8);
					stoplist.add(new String(tmp, UTF_8));
				}
			}

			br.close();
			r.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * Analyzer a specific news
	 * 
	 * @param f
	 * @return
	 * @throws IOException
	 */
	protected News analyzerNews(File f) throws IOException {
		News news = null;
		Reader reader = new FileReader(f);
		BufferedReader bRead = new BufferedReader(reader);

		String tilte = bRead.readLine();
		String line;

		List<NamedEntity> listEntity = new ArrayList<>();
		while ((line = bRead.readLine()) != null) {
			listEntity.addAll(getEntities(line));
		}

		bRead.close();

		news = new News(tilte, listEntity);
		return news;
	}

	private Collection<? extends NamedEntity> getEntities(String lines) {
		List<NamedEntity> list = new ArrayList<>();

		StringTokenizer tk = new StringTokenizer(lines, " ");

		while (tk.hasMoreElements()) {
			String e = (String) tk.nextElement();
			String e1 = e.toLowerCase();

			if (Character.isUpperCase(e.charAt(0)) && !stoplist.contains(e1))
				list.add(new NamedEntity(e));
		}
		return list;
	}
}
