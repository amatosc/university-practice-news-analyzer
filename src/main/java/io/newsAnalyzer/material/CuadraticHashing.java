package io.newsAnalyzer.material;

public class CuadraticHashing<K, V> extends HashTableMap<K, V> {

	public CuadraticHashing(int p, int cap) {
		super(p, cap);
	}

	public CuadraticHashing() {
		super();
	}

	public CuadraticHashing(int cap) {
		super(cap);
	}

	protected int hashValue(K key, int i) {
		int c1, c2, c3;
		c1 = 31;
		c2 = 97;

		c3 = (c1 * i) + (c2 * (i * i));

		return (hashValue(key) + c3) % capacity;
	}

	private HashEntryIndex findEntryPUT(K key) throws InvalidKeyException {
		HashEntryIndex tmp = findEntry(key);
		if (tmp.index < 0) {
			rehash();
			return findEntryPUT(key);
		} else
			return tmp;
	}

	@Override
	public V put(K key, V value) throws InvalidKeyException {
		HashEntryIndex i = findEntryPUT(key); // find the appropriate spot for
												// this
												// entry
		if (i.operation == OperationType.found) // this key has a previous value
		{
			return bucket[i.index].setValue(value); // set new value
		} else if (n >= capacity / 2) {
			rehash(); // rehash to keep the load factor <= 0.5
			i = findEntryPUT(key); // find again the appropriate spot for this
									// entry

		}

		bucket[i.index] = new HashEntry<>(key, value); // convert to proper
		// index
		n++;
		return null; // there was no previous value
	}

	protected HashEntryIndex findEntry(K key) throws InvalidKeyException {
		int avail = -1;
		checkKey(key);
		int try_on = 0;
		int i = hashValue(key, try_on);
		do {
			Entry<K, V> e = bucket[i];
			if (e == null) {
				if (avail < 0) {
					avail = i; // key is not in table
				}
				break;
			} else if (key.equals(e.getKey())) // we have found our key
			{
				return new HashEntryIndex(i, OperationType.found); // key found
			} else if (e == AVAILABLE) { // bucket is deactivated
				if (avail < 0) {
					avail = i; // remember that this slot is available
				}
			}
			try_on++;
			i = hashValue(key, try_on); // keep looking
		} while (try_on < capacity);

		return new HashEntryIndex(avail, OperationType.notFound); // first empty
																	// or
																	// available
																	// slot
	}

}
