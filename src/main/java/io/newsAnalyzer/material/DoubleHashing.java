package io.newsAnalyzer.material;

import java.util.ArrayList;
import java.util.List;

public class DoubleHashing<K, V> extends HashTableMap<K, V> {

	/*
	 * Prime number for Double hashing.
	 */
	private int q;

	public DoubleHashing(int p, int cap) {
		super(p, cap);

		q = getPrime(capacity);
	}

	public DoubleHashing() {
		super();

		q = getPrime(capacity);
	}

	public DoubleHashing(int cap) {
		super(cap);

		q = getPrime(capacity);
	}

	private int hashValue(K key, int i) {
		int desp = getDes(key) * i;
		return (hashValue(key) + desp) % capacity;
	}

	@Override
	protected HashEntryIndex findEntry(K key) throws InvalidKeyException {
		int avail = -1;
		checkKey(key);
		int try_on = 0;
		int i = hashValue(key, try_on);
		do {
			Entry<K, V> e = bucket[i];
			if (e == null) {
				if (avail < 0) {
					avail = i; // key is not in table
				}
				break;
			} else if (key.equals(e.getKey())) // we have found our key
			{
				return new HashEntryIndex(i, OperationType.found); // key found
			} else if (e == AVAILABLE) { // bucket is deactivated
				if (avail < 0) {
					avail = i; // remember that this slot is available
				}
			}
			try_on++;
			i = hashValue(key, try_on); // keep looking
		} while (try_on < capacity);
		return new HashEntryIndex(avail, OperationType.notFound); // first empty
																	// or
																	// available
																	// slot
	}

	private HashEntryIndex findEntryPUT(K key) throws InvalidKeyException {
		HashEntryIndex tmp = findEntry(key);
		if (tmp.index < 0) {
			q = getPrime(capacity * 2);
			rehash();
			return findEntryPUT(key);
		} else
			return tmp;
	}

	@Override
	public V put(K key, V value) throws InvalidKeyException {
		HashEntryIndex i = findEntryPUT(key); // find the appropriate spot for
												// this
												// entry
		if (i.operation == OperationType.found) // this key has a previous value
		{
			return bucket[i.index].setValue(value); // set new value
		} else if (n >= capacity / 2) {
			q = getPrime(capacity * 2);
			rehash(); // rehash to keep the load factor <= 0.5
			i = findEntryPUT(key); // find again the appropriate spot for this
									// entry

		}

		bucket[i.index] = new HashEntry<>(key, value); // convert to proper
		// index
		n++;
		return null; // there was no previous value
	}

	/**
	 * Returns the first prime number less than N.
	 * 
	 * @return
	 */
	private int getPrime(int max) {
		List<Integer> list = new ArrayList<>(max - 2);
		for (int i = 2; i < max - 1; i++)
			list.add(i);

		for (int i = 0; i < list.size(); i++) {
			int n = list.get(i);
			for (int j = i + 1; j < list.size(); j++) {
				if (list.get(j) % n == 0)
					list.remove(j);
			}
		}

		if (max <= 2) {
			return 2;
		}

		return list.get(list.size() - 1);
	}

	/**
	 * Gives the shift.
	 * 
	 * @param key
	 * @return
	 */
	private int getDes(K key) {
		return q - (key.hashCode() % capacity);
	}
}
