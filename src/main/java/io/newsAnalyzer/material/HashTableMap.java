package io.newsAnalyzer.material;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public abstract class HashTableMap<K, V> {

	/**
	 * Creates a hash table with prime factor 109345121 and capacity 1000.
	 */
	public HashTableMap() {
		this(109345121, 1000); // reusing the constructor HashTableMap(int p,
								// int cap)
	}

	/**
	 * Creates a hash table with prime factor 109345121 and given capacity.
	 * 
	 * @param cap
	 *            initial capacity
	 */
	public HashTableMap(int cap) {
		this(109345121, cap); // reusing the constructor HashTableMap(int p, int
								// cap)
	}

	/**
	 * Creates a hash table with the given prime factor and capacity.
	 * 
	 * @param p
	 *            prime number
	 * @param cap
	 *            initial capacity
	 */
	public HashTableMap(int p, int cap) {
		this.n = 0;

		this.prime = p;
		this.capacity = cap;

		this.bucket = (HashEntry<K, V>[]) new HashEntry[capacity]; // safe cast
		Random rand = new Random();
		this.scale = rand.nextInt(prime - 1) + 1;
		this.shift = rand.nextInt(prime);
	}

	public enum OperationType {
		found, notFound
	};

	protected class HashEntry<K, V> implements Entry<K, V> {

		protected K key;
		protected V value;

		public HashEntry(K k, V v) {
			key = k;
			value = v;
		}

		public V getValue() {
			return value;
		}

		public K getKey() {
			return key;
		}

		public V setValue(V val) {
			V oldValue = value;
			value = val;
			return oldValue;
		}

		public boolean equals(Object o) {

			if (o.getClass() != this.getClass())
				return false;

			HashEntry<K, V> ent = (HashEntry<K, V>) o;

			return (ent.getKey().equals(this.key))
					&& (ent.getValue().equals(this.value));
		}

		/**
		 * Entry visualization.
		 */

		public String toString() {
			return "(" + key + "," + value + ")";
		}
	}

	protected class HashEntryIndex {

		int index;
		OperationType operation;

		public HashEntryIndex(int index, OperationType operation) {
			this.index = index;
			this.operation = operation;
		}
	}

	protected final Entry<K, V> AVAILABLE = new HashEntry<>(null, null);
	protected int n = 0; // number of entries in the dictionary

	protected int prime, capacity; // prime factor and capacity of bucket array
	protected HashEntry<K, V>[] bucket; // bucket array
	protected long scale, shift; // the shift and scaling factors

	/**
	 * Put a key-value pair in the map, replacing previous one if it exists.
	 * 
	 * @param key
	 * @param value
	 * @return
	 */

	public abstract V put(K key, V value) throws InvalidKeyException;

	/**
	 * Determines whether a key is valid.
	 * 
	 * @param k
	 *            Key
	 */
	protected void checkKey(K k) {
		// We cannot check the second test (i.e., k instanceof K) since we do
		// not know the class K
		if (k == null) {
			throw new InvalidKeyException("Invalid key: null.");
		}
	}

	/**
	 * Hash function applying MAD method to default hash code.
	 * 
	 * @param key
	 *            Key
	 * @return
	 */
	protected int hashValue(K key) {
		return (int) ((Math.abs(key.hashCode() * scale + shift) % prime) % capacity);
	}

	/**
	 * Returns the number of entries in the hash table.
	 * 
	 * @return the size
	 */
	public int size() {
		return n;
	}

	/**
	 * Returns whether or not the table is empty.
	 * 
	 * @return true if the size is 0
	 */

	public boolean isEmpty() {
		return (n == 0);
	}

	/**
	 * Returns an iterable object containing all of the keys.
	 * 
	 * @return
	 */

	public Iterable<K> keySet() {
		List<K> keys = new ArrayList<>();
		for (int i = 0; i < capacity; i++) {
			if ((bucket[i] != null) && (bucket[i] != AVAILABLE)) {
				keys.add(bucket[i].getKey());
			}
		}
		return keys;
	}

	/**
	 * Collision solved with linear probe - returns index of found key or -(a +
	 * 1), where a is * the index of the first empty or available slot found.
	 * The index value is negative because it is needed to distinguish when the
	 * key is in the table (positive) and when is not (negative)
	 * 
	 * @param key
	 * @return
	 */
	protected abstract HashEntryIndex findEntry(K key)
			throws InvalidKeyException;

	/**
	 * Returns the value associated with a key.
	 * 
	 * @param key
	 * @return
	 */

	public V get(K key) throws InvalidKeyException {
		HashEntryIndex i = findEntry(key); // helper method for finding a key
		if (i.operation == OperationType.notFound) {
			return null; // there is no value for this key, so return null
		}
		return bucket[i.index].getValue(); // return the found value in this
											// case
	}

	/**
	 * Doubles the size of the hash table and rehashes all the entries.
	 */
	protected void rehash() {
		HashEntry<K, V>[] tmp = bucket;
		capacity *= 2;
		n = 0;
		bucket = (HashEntry<K, V>[]) new HashEntry[capacity];

		for (int i = 0; i < tmp.length; i++) {
			if (tmp[i] != null)
				this.put(tmp[i].getKey(), tmp[i].getValue());
		}

	}

	/**
	 * Removes the key-value pair with a specified key.
	 * 
	 * @param key
	 * @return
	 */

	public V remove(K key) throws InvalidKeyException {
		HashEntryIndex i = findEntry(key); // find this key first
		if (i.operation == OperationType.notFound) {
			return null; // nothing to remove
		}
		V toReturn = bucket[i.index].getValue();
		bucket[i.index] = (HashEntry<K, V>) AVAILABLE; // mark this slot as
														// reactivated
		n--;
		return toReturn;
	}

	/**
	 * Returns an iterable object containing all of the entries.
	 * 
	 * @return
	 */

	public Iterable<Entry<K, V>> entrySet() {
		List<Entry<K, V>> entries = new ArrayList<>();
		for (int i = 0; i < capacity; i++) {
			if ((bucket[i] != null) && (bucket[i] != AVAILABLE)) {
				entries.add(bucket[i]);
			}
		}
		return entries;
	}

	/**
	 * Returns an iterable object containing all of the values.
	 * 
	 * @return
	 */

	public Iterable<V> values() {
		List<V> values = new ArrayList<>();
		for (int i = 0; i < capacity; i++) {
			if ((bucket[i] != null) && (bucket[i] != AVAILABLE)) {
				values.add(bucket[i].getValue());
			}
		}
		return values;
	}
}
