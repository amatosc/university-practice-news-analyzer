package io.newsAnalyzer.material;

/**
 * A hash table with linear probing and the MAD hash function
 * 
 * 
 * A hash table data structure that uses linear probing to handle collisions.
 * The hash function uses the built-in hashCode method and the
 * multiply-add-and-divide method. The load factor kept less than or equal to
 * 0.5. When the load factor reaches 0.5, the entries are rehashed into a new
 * bucket array with twice the capacity.
 * */

public class LinearHashing<V, K> extends HashTableMap<K, V> {

	public LinearHashing(int p, int cap) {
		super(p, cap);
	}

	/*
	 * public LinearHashing(ColisionSolver c) { super(); }
	 */

	public LinearHashing() {
		super();
	}

	public LinearHashing(int cap) {
		super(cap);
	}

	@Override
	protected HashEntryIndex findEntry(K key) throws InvalidKeyException {
		int avail = -1;
		checkKey(key);
		int i = hashValue(key);
		final int j = i;
		do {
			Entry<K, V> e = bucket[i];
			if (e == null) {
				if (avail < 0) {
					avail = i; // key is not in table
				}
				break;
			} else if (key.equals(e.getKey())) // we have found our key
			{
				return new HashEntryIndex(i, OperationType.found); // key found
			} else if (e == AVAILABLE) { // bucket is deactivated
				if (avail < 0) {
					avail = i; // remember that this slot is available
				}
			}
			i = (i + 1) % capacity; // keep looking
			// int index = colisionSolver.get(i,key);
		} while (i != j);
		return new HashEntryIndex(avail, OperationType.notFound); // first empty
																	// or
																	// available
																	// slot
	}

	@Override
	public V put(K key, V value) throws InvalidKeyException {
		HashEntryIndex i = findEntry(key); // find the appropriate spot for this
		// entry
		if (i.operation == OperationType.found) // this key has a previous value
		{
			return bucket[i.index].setValue(value); // set new value
		} else if (n >= capacity / 2) {
			rehash(); // rehash to keep the load factor <= 0.5
			i = findEntry(key); // find again the appropriate spot for this
			// entry
		}

		bucket[i.index] = new HashEntry<>(key, value); // convert to proper
		// index
		n++;
		return null; // there was no previous value
	}
}
